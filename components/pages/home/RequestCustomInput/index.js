import React,{useState} from 'react'
import Style from './style'
import TextareaAutosize from 'react-autosize-textarea';
const RequestCustomInput =(props)=>{

    return(
        <Style>
            <div className='custom-input-request'>
            <TextareaAutosize {...props} />
            </div>
        </Style>
)
}
export default RequestCustomInput