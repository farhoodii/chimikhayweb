import styled from 'styled-components'
const Style = styled.div`
    .upload-box{
        width:100%;
        display:flex;
        justify-content:flex-end;
        height:50px;
        align-items:flex-end;
        i{
            font-size: 30px;
            color:white;
        }
        i:hover{
            cursor:pointer;
        }
    }
    `;
export default Style