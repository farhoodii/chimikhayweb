import styled from 'styled-components'
const Style = styled.div`
.ant-form-horizontal{
    direction:rtl !important;
    .ant-form-item{
        padding-bottom:10px;
    }
    label{
        font-size:14px;
        font-weight:bold;
    }
}
input{
    height: 42px;
}
`;
export default Style