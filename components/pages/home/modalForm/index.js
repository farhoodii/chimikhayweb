import React,{useState} from 'react'
import { Form, Input, Button } from 'antd';
import Style from './style'

const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };
const ModalForm =(props)=>{

    const onFinish = values => {
        console.log('Success:', values);
      };
    
      const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
      };
    
      return (
        <Style>

        <Form
          {...layout}
          name="basic"
          form={props.form}
          initialValues={{
            // remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          >
           
          <Form.Item
            label=" اسم"
            name="firstName"
            rules={[
              {
                required: true,
                message: 'لطفا اسم خودت رو وارد کن!',
              },
            ]}
            >
            <Input />
          </Form.Item>

         
    
          <Form.Item
            label=" فامیلی"
            name="lastName"
            rules={[
              {
                required: true,
                message: 'لطفا فامیلیت رو وارد کن!',
              },
            ]}
            >
            <Input/>
          </Form.Item>

          <Form.Item
            label=" شماره تماس"
            name="phoneNumber"
            rules={[
              {
                required: true,
                message: 'لطفا شماره تماس خودت رو بهمون بده!',
              },
            ]}
            >
            <Input />
          </Form.Item>
    
        </Form>
        </Style>
      );

}
export default ModalForm