import React,{useState} from 'react'
import Style from './style'
import RequestModal from '../requestModal'

const SubmitBtn =(props)=>{
    const [visible,changeVisibility]=useState(false)
    const {onSubmit}=props
    return(
        <Style>
            <div className='request-submit-btn'>
            <button onClick={()=>changeVisibility(true)} > ثبت  </button>
            </div>
            <div>
            </div>
            {visible?
            <RequestModal onSubmit={onSubmit} changeVisibility={changeVisibility} visible={visible}/>
            :null}
        </Style>
)
}
export default SubmitBtn