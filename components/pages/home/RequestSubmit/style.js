import styled from 'styled-components'
const Style = styled.div`
button{
    width:100%;
    height:50px;
    text-align:center;
    background: white;
    border-radius: 5px;
    border: 1px solid gray;
    color:#148490;
    font-weight:bold;
    font-size:20px;
}
button:hover{
    cursor:pointer;
}
    `;
export default Style