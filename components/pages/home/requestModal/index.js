import React from 'react';
import { Modal,Form } from 'antd';
import ModalForm from '../modalForm';

const RequestModal=(props)=>{
    const [form] = Form.useForm();
    const {changeVisibility,onSubmit,visible}=props;
    return(
        <Modal
        title="ثبت نهایی"
        visible={visible}
        // onOk={()=>props.changeVisibility(false)}
        onCancel={()=>changeVisibility(false)}
        okText={"حله"}
        cancelText={"بیخیال"}
        onOk={() => {
            form
              .validateFields()
              .then(values => {
                form.resetFields();
                onSubmit(values)
                changeVisibility(false)
              })
              .catch(info => {
                console.log('Validate Failed:', info);
              });
          }}
      >
       <ModalForm form={form}/>
      </Modal>
    )
}

export default RequestModal