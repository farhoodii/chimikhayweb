import styled from 'styled-components'
const Style = styled.div`
    position:absolute;
    width:100%;
    height:100%;
    img{
        width:80px;
        height:80px;
        position:absolute;
    }
    .one{
        left:15%;
        top:22%;
        transform: rotate(20deg);
    }
    .two{
        left:15%;
        top:65%;
        transform: rotate(-20deg);
    }
    .thre{
        left:30%;
        top:30%;
        transform: rotate(20deg);
    }
    .eight{
        left:55%;
        top:80%;
        transform: rotate(40deg);
    }
    .four{
        left:40%;
        top:70%;
        transform: rotate(40deg);
    }
    .five{
        left:80%;
        top:70%;
        transform: rotate(-10deg);
    }
    .six{
        left:80%;
        top:30%;
        transform: rotate(15deg);
    }
    .seven{
        left:55%;
        top:20%;
    }
    @keyframes shake {
        0% { transform: translate(1px, 1px) rotate(0deg); }
        10% { transform: translate(-1px, -2px) rotate(-1deg); }
        20% { transform: translate(-3px, 0px) rotate(1deg); }
        30% { transform: translate(3px, 2px) rotate(0deg); }
        40% { transform: translate(1px, -1px) rotate(1deg); }
        50% { transform: translate(-1px, 2px) rotate(-1deg); }
        60% { transform: translate(-3px, 1px) rotate(0deg); }
        70% { transform: translate(3px, 1px) rotate(-1deg); }
        80% { transform: translate(-1px, -1px) rotate(1deg); }
        90% { transform: translate(1px, 2px) rotate(0deg); }
        100% { transform: translate(1px, -2px) rotate(-1deg); }
      }

      img:hover{
        animation: shake 0.5s;
        animation-iteration-count: infinite;
      }
      
    `;
export default Style