import React,{useState} from 'react'
import socks from '../../public/background/socks.png'
import jacket from '../../public/background/jacket.png'
import handmade from '../../public/background/handmade.png'
import harness from '../../public/background/harness.png'
import art from '../../public/background/art.png'
import africa from '../../public/background/africa.png'
import combat from '../../public/background/combat.png'
import skateboard from '../../public/background/skateboard.png'

import Style from './style'

const Layout =(props)=>{
    return(
        <Style>
            <img className='one' src={socks} />
            <img className='two' src={jacket} />
            <img className='thre' src={handmade} />
            <img className='four' src={art} />
            <img className='five' src={harness} />
            <img className='six' src={africa} />
            <img className='seven' src={combat} />
            <img className='eight' src={skateboard} />
            {props.children}
        </Style>
)
}
export default Layout