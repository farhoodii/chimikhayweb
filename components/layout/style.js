import styled from 'styled-components'
const Style = styled.div`
    position:fixed;
    top:0px;
    right:0px;
    width:100%;
    height:90px;
    background:#03010126;
    z-index:99;
    .menus{
        display:flex;
        height:90px;
        align-items:center;
        a{
            p{
                color:white;
                font-size:1.5rem;
            }
            margin-right:30px;
        }
        a:first-child {
            margin-right:unset;            
        }
    }
    `;
export default Style