import styled from 'styled-components'
const Style = styled.div`
.home-container{
    width:100%;
    height:100vh;
    .title{
        h1{
            color:white;
        }
    }
    .input-box{
        width:100%;
        height:100vh;
        justify-content:center;
        align-items:center;
        flex-direction:column;
        display:flex;
        .input{
            width:800px;
        }
        .sub-boxes{
            width:800px;
            display:flex;
            flex-direction:row-reverse;
            justify-content:space-between;
            .submit{
                width:150px;
            }
            .upload{width:80px;}
        }
    }
}
`;
export default Style