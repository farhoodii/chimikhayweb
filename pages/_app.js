import React from 'react'
import App from 'next/app'
import GlobalStyle from '../styles/globalStyle'
import {MyContext} from '../context'
import Alert from '../components/alert'
import 'antd/dist/antd.css';

class MyApp extends App {

  render() {
    const { Component, pageProps } = this.props
    return(
      <MyContext.Provider value={{}}>
        <GlobalStyle>
            <Alert/>
            <Component {...pageProps} />
            <style global jsx>{`
            body {
            margin: 0;
            }
            `}</style>
        </GlobalStyle>
        </MyContext.Provider>
        )
  }
}

export default MyApp