import React,{useState, useEffect} from 'react'
import Head from 'next/head'
import Style from '../styles/homeStyle'
import RequestCustomInput from '../components/pages/home/RequestCustomInput'
import UploadBox from '../components/pages/home/UploadBox'
import BackgroundIcons from '../components/backgroundIcons'
import {ContextWrapper} from '../context'
import RequestSubmitBtn from '../components/pages/home/RequestSubmit'

const Home = () => {
  const [formData,changeFormData]=useState({description:null,firstName:null,lastName:null,phoneNumber:null});

  const handleDescriptionChange=(description)=>{
    changeFormData({...formData,description:description.target.value})
  }

  const onFormSubmit=(data)=>{
    // changeFormData({...formData,...data})
    // if(!description||description===''){
    //   props.setAlert()
    // }
    changeFormData({...formData,...data,description:''})
  }

  useEffect(()=>{
    console.log(formData,'form data')
  })

  const {description}=formData;

  return (
  <Style>
    <Head>
      <title>Home</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <div className='home-container'>
      <BackgroundIcons>
      <div className='input-box'>
        <div className='title'>
          <h1>!!!چیزی که میخای رو بهمون</h1>
        </div>
        <div className='input'>
        <RequestCustomInput value={description} onChange={handleDescriptionChange}/>
        </div> 
        <div className='sub-boxes'>
          <div className='upload'>
            <UploadBox/>
          </div>
          <div className='submit'>
            <RequestSubmitBtn onSubmit={onFormSubmit}/>
          </div>
        </div>
      </div>
      </BackgroundIcons>
    </div>  
  </Style>
)
}
export default ContextWrapper(Home) 
