import React from 'react'

export const MyContext = React.createContext({});

export const ContextWrapper=(Component)=>{
    return( 
        class Injector extends React.Component{
        constructor(props) {
            super(props);
            this.state={
                alert:false
            }
        }
        setAlert=()=>{
            this.setState({alert:true})
        }
        render(){
            return(
                <MyContext.Consumer>
                {value=>{
                    return(
                    <Component 
                     setAlert={()=>this.setAlert()}
                     {...this.props} {...this.state} context={value}
                    />
                    )
                }}
                </MyContext.Consumer>
                )
            }
            
        }
    )
}




